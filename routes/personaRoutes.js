const { Router } = require('express');
const {
  APIpokemonGet,
  covidGet,
  jsonPlaceholderGet,

} = require('../controllers/personaController');

const router = Router();

router.get('/1', APIpokemonGet);

router.get('/2', covidGet);

router.get('/3', jsonPlaceholderGet);



module.exports = router;