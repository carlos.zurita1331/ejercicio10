const express = require('express');
const axios = require('axios');

class Server {


  constructor(){
    this.app = express();
    this.port = process.env.PORT;

    this.personaPath = '/api/persona';

    this.middlewares();

    this.routes();
  }

  middlewares() {
    //Lectura y aplicaion de la informacion
    this.app.use(express.json());
    this.app.use(express.static('public'));
  }
   routes() {
     this.app.use(this.personaPath, require('../routes/personaRoutes'));
  }

   listen() {
    //Con process.env.PORT obtenemos el valor de la variable PORT que esta en el archivo .env
    this.app.listen(this.port, () => {
    console.log('Servidor corriendo en puerto ', this.port);
    });
  }

}

module.exports = Server;