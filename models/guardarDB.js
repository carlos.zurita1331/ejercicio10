
const fs = require('fs');
const archivo = './resultado-endpoints/sitio1.txt';
const archivo2 = './resultado-endpoints/sitio2.txt';
const archivo3 = './resultado-endpoints/sitio3.txt';

const guardar = (data) => {

  fs.writeFileSync(archivo, JSON.stringify(data))
  //convierte en cadena la data en datos para almacenar
}

const guardar2API = (data) => {

  fs.writeFileSync(archivo2, JSON.stringify(data))
  //convierte en cadena la data en datos para almacenar
}


const guardar3API = (data) => {

  fs.writeFileSync(archivo3, JSON.stringify(data))
  //convierte en cadena la data en datos para almacenar
}

const leerInfo = () => {
  if(!fs.existsSync(archivo)){
    return null;
   }
   console.log(archivo);
   const info = fs.readFileSync(archivo, {encoding: 'utf-8'})
  console.log('ifo', info, 'info');
   //contenido es array de json en string

   const data = JSON.parse(info);

  console.log(data, 'data ');
   return data
}


module.exports = {
  guardar,
  leerInfo,
  guardar2API,
  guardar3API
};