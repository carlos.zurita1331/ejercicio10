
const axios = require('axios');
const { guardar, guardar2API, guardar3API } = require("../models/guardarDB");
const { response } = require("express");

const link1 = 'https://pokeapi.co/api/v2/pokemon/';
const link2 = 'https://covidtracking.com/data/api';
const link3 = 'https://jsonplaceholder.typicode.com/todos';

const API1 = []
const API2 = []
const API3 = []

const APIpokemonGet = async (req, res = response) => {

  try{
    const respuestaAPI = await axios.get(link1);
    const response = respuestaAPI.data.results;

    Object.keys(response).forEach(key => {
      const dato= response[key];
      console.log(dato);
      API1.push(dato)
});
  guardar(API1)
    
    res.json({
      response
    });
  } catch (error) {
    console.log(error);
    
    res.json({
      error: 'No se consumio el api rest'
    });
  }

};


const covidGet = async (req, res = response) => {

  try{
    const respuestaAPI = await axios.get(link2);
    const response = respuestaAPI.data;

    Object.keys(response).forEach(key => {
      const dato= response[key]
      console.log(dato);
      API2.push(dato)
});
  guardar2API(API2)
    
    res.json({
      response
    });
  } catch (error) {
    console.log(error);
    
    res.json({
      error: 'No se consumio el api rest'
    });
  }

};


const jsonPlaceholderGet = async (req, res = response) => {

  try{
    const respuestaAPI = await axios.get(link3);
    const response = respuestaAPI.data;
    // console.log(response);

    Object.keys(response).forEach(key => {
      const dato= response[key]
      API3.push(dato)
});
  guardar3API(API3)
    
    res.json({
      response
    });
  } catch (error) {
    console.log(error);
    
    res.json({
      error: 'no se consumio el api rest'
    });
  }

};


module.exports = {
  APIpokemonGet,
  covidGet,
  jsonPlaceholderGet
}